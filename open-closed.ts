//Open Closed Principle
//Dependency Inversion
//Composite

/* 
Maneira Errada
class AddGroupController {
  constructor(
    private requiredFieldValidation: RequiredFieldValidation,
    private phoneValidation: PhoneValidation
  ) {}
  add() {
    this.requiredFieldValidation.validate();
  }
}

class RequiredFieldValidation {
  validate() {}
}

class PhoneValidation {}

const controller = new AddGroupController();
*/

class AddGroupController {
  constructor(private validation: Validation) {}
  add() {
    this.validation.validate();
  }
}

interface Validation {
  validate: () => void;
}

class RequiredFieldValidation implements Validation {
  validate(): void {}
}

class PhoneValidation implements Validation {
  validate(): void {}
}

class AddGroupValidationComposite implements Validation {
  constructor(
    private requiredFieldValidation: RequiredFieldValidation,
    private phoneValidation: PhoneValidation
  ) {}
  validate(): void {}
}

//Composition Root
//Depency Injection
const phoneValidation = new PhoneValidation();
const requiredFieldValidation = new RequiredFieldValidation();
const composite = new AddGroupValidationComposite(
  requiredFieldValidation,
  phoneValidation
);
const controller = new AddGroupController(composite);
