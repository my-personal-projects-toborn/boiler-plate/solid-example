import express from "express";
import { MongoHelper } from "./mongo-helper";

const app = express();

app.post("/", async (req, res) => {
  const userModel = await MongoHelper.instance.getCollection("users");
  const usuario = userModel.insertOne({ name: "Marcio" });
  res.send({ message: "Usuario criado!" });
});

app.get("/", async (req, res) => {
  const userModel = await MongoHelper.instance.getCollection("users");
  const usuarios = userModel.find().toArray();
  res.send({ usuarios });
});

app.listen(5000, () => console.log("server running"));
