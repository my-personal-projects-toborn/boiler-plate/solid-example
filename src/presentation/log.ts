export interface Log {
  event: (type: Log.Type, data: any) => void;
}

export namespace Log {
  export type Type = "error" | "action" | "screen";

  export type Data = {
    error?: any;
    name?: string;
  };
}
