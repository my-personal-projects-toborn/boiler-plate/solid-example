//factory
import { LogStrategy } from "../analytics/log-strategy";
import { LoginView } from "../presentation/login-view";
import { ActionLog } from "../analytics/action-log";
import { ScreenLog } from "../analytics/screen-log";
import { ErrorLog } from "../analytics/error-log";
import { FirebaseAdapter } from "../infra/firebase-adapter";
import { SentryAdapter } from "../infra/sentry-adapter";
import { LogglyAdapter } from "../infra/loggly-adapter";
import { ErrorAnalyticsComposite } from "../infra/error-analytics-composite";
import { MongoAnalyticsRepository } from "../infra/mongo-analytics-repo";

export const makeLoginView = (): LoginView => {
  const errorAnalyticsComposite = new ErrorAnalyticsComposite([
    new SentryAdapter(),
    new LogglyAdapter(),
    new MongoAnalyticsRepository(),
  ]);
  const firebaseAdapter = new FirebaseAdapter();
  const screenLog = new ScreenLog(firebaseAdapter);
  const actionLog = new ActionLog(firebaseAdapter);
  const errorLog = new ErrorLog(errorAnalyticsComposite);
  const logStrategy = new LogStrategy(errorLog, screenLog, actionLog);
  return new LoginView(logStrategy);
};
